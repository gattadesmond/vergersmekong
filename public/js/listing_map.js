(function(A) {
  if (!Array.prototype.forEach)
    A.forEach =
      A.forEach ||
      function(action, that) {
        for (var i = 0, l = this.length; i < l; i++)
          if (i in this) action.call(that, this[i], i, this);
      };
})(Array.prototype);

var mapObject,
  markers = [],
  markersData = {
    Marker: [
      {
        name: "11 Th\u00e1i Thu\u1eadn",
        google_address:
          "11 Th\u00e1i Thu\u1eadn, An Phu, District 2, Ho Chi Minh City, Vietnam",
        location_latitude: "10.7986269",
        location_longitude: "106.74062070000002",
        country: "Vietnam",
        area: "Ho Chi Minh",
        address:
          "11 Thai Thuan, An Phu Ward, District 2, Ho Chi Minh City, Vietnam",
        website: "",
        url:
          "https://maps.google.com/?q=11+Th%C3%A1i+Thu%E1%BA%ADn,+Th%E1%BA%A3o+%C4%90i%E1%BB%81n,+Qu%E1%BA%ADn+2,+H%E1%BB%93+Ch%C3%AD+Minh,+Vietnam&ftid=0x31752611f1287345:0x88b9a5626eda17a5",
        email: "info@vergersmekong.com.vn",
        no_address: false
      },
      {
        name: "An Hai",
        google_address:
          "An H\u1ea3i 5, S\u01a1n Tr\u00e0, \u0110\u00e0 N\u1eb5ng, Vietnam",
        location_latitude: "16.0828413",
        location_longitude: "108.23195850000002",
        country: "Vietnam",
        area: "Da Nang",
        address:
          "#14, Group 94, An Hai 5 street, An Hai Bac ward, Son Tra District, Da Nang City, Vietnam",
        website: "",
        url:
          "https://maps.google.com/?q=An+H%E1%BA%A3i+5,+An+H%E1%BA%A3i,+An+H%E1%BA%A3i+B%E1%BA%AFc,+S%C6%A1n+Tr%C3%A0,+%C4%90%C3%A0+N%E1%BA%B5ng+550000,+Vietnam&ftid=0x31421821802d731b:0x1ddc37d7a2c25245",
        email: "info@vergersmekong.com.vn",
        no_address: false
      },
      {
        name: "Ben Tram",
        google_address: "Ph\u00fa Qu\u1ed1c, Phu Quoc, Kien Giang, Vietnam",
        location_latitude: "10.289879",
        location_longitude: "103.98401999999999",
        country: "Vietnam",
        area: "Kien Giang",
        address: "Ben Tram, Cuu Duong, Phu Quoc, Kien Giang province, Vietnam",
        website: "",
        url:
          "https://maps.google.com/?q=Ph%C3%BA+Qu%E1%BB%91c&ftid=0x31a78c62b49eda17:0x8aa79fbbdd72cdb",
        email: "info@vergersmekong.com.vn",
        no_address: false
      },
      {
        name: "Khan Russey Keo",
        google_address: "Khan Russey Keo, Phnom Penh, Cambodia",
        location_latitude: "11.6164769",
        location_longitude: "104.89800579999996",
        country: "Cambodia",
        area: "Phnom Penh",
        address:
          "#G267,Street 89R,Phum Khlaing Saing,Sangkat Reussey Keo,Khan Reussey keo , Phnom Penh, Cambodia",
        website: "",
        url:
          "https://maps.google.com/?q=Khan+Russey+Keo,+Phnom+Penh,+Cambodia&ftid=0x310953403bc52d1f:0x2467d86f511cb978",
        email: "info@vergersmekong.com.vn",
        no_address: false
      },
      {
        name: "Khu C\u00f4ng Nghi\u1ec7p Tr\u00e0 N\u00f3c",
        google_address:
          "Khu C\u00f4ng Nghi\u1ec7p Tr\u00e0 N\u00f3c, B\u00ecnh Th\u1ee7y, C\u1ea7n Th\u01a1, Vietnam",
        location_latitude: "10.1069139",
        location_longitude: "105.70857460000002",
        country: "Vietnam",
        area: "Can Tho",
        address:
          "Lot 17E1, street #5, Tra Noc 1 Industrial Zone, Binh Thuy district, Can Tho city, Vietnam",
        website: "",
        url:
          "https://maps.google.com/?q=Khu+C%C3%B4ng+Nghi%E1%BB%87p+Tr%C3%A0+N%C3%B3c,+B%C3%ACnh+Th%E1%BB%A7y,+C%E1%BA%A7n+Th%C6%A1,+Vietnam&ftid=0x31a0868e255ff1ad:0xed7f407843d803fe",
        email: "info@vergersmekong.com.vn",
        no_address: false
      },
      {
        name: "Malaysia",
        google_address: "Malaysia",
        location_latitude: "4.210483999999999",
        location_longitude: "101.97576600000002",
        country: "Malaysia",
        area: "Malaysia",
        address: "Malaysia",
        website: "http://www.malaysia.gov.my/",
        url:
          "https://maps.google.com/?q=Malaysia&ftid=0x3034d3975f6730af:0x745969328211cd8",
        email: "export@vergersmekong.com.vn",
        no_address: true
      },
      {
        name: "Ngoc Thuy",
        google_address:
          "Ng\u1ecdc Th\u1ee5y, Long Bi\u00ean, H\u00e0 N\u1ed9i, Vietnam",
        location_latitude: "21.0588582",
        location_longitude: "105.8583873",
        country: "Vietnam",
        area: "Hanoi",
        address:
          "No. 1, Path #75, Ngoc Thuy Road, Long Bien, Ha Noi City, Vietnam",
        website: "",
        url:
          "https://maps.google.com/?q=Ngoc+Thuy,+Hanoi,+Vietnam&ftid=0x3135aa2e4261c55b:0x8de2243c2dffb31a",
        email: "info@vergersmekong.com.vn",
        no_address: false
      },
      {
        name: "Phuoc Long",
        google_address:
          "S\u1ed1 2, Ph\u01b0\u1edbc Long, Nha Trang, Th\u00e0nh ph\u1ed1 Nha Trang, Khanh Hoa Province, Vietnam",
        location_latitude: "12.2164381",
        location_longitude: "109.19046159999994",
        country: "Vietnam",
        area: "Khanh Hoa",
        address: "21, Street #2, Phuoc Long ward, Nha Trang city, Vietnam",
        website: "",
        url:
          "https://maps.google.com/?q=S%E1%BB%91+2,+Ph%C6%B0%E1%BB%9Bc+Long,+Nha+Trang,+Tp.+Nha+Trang,+Kh%C3%A1nh+H%C3%B2a,+Vietnam&ftid=0x31706758f93f2549:0x6b522850ee9e290a",
        email: "info@vergersmekong.com.vn",
        no_address: false
      },
      {
        name: "Sangkat Svay Dangkum",
        google_address:
          "Sangkat Svay Dangkum, Krong Siem Reap, Siem Reap Province, Cambodia",
        location_latitude: "13.3412453",
        location_longitude: "103.82871130000001",
        country: "Cambodia",
        area: "Siem Reap Province",
        address:
          "Sivutha Street, Salakonseng village, Sangkat Svaydongkum, Siem Reap province, Cambodia",
        website: "",
        url:
          "https://maps.google.com/?q=Sangkat+Svay+Dangkum,+Krong+Siem+Reap,+Cambodia&ftid=0x311017422dda31c1:0x21ca2808451d3163",
        email: "info@vergersmekong.com.vn",
        no_address: false
      },
      {
        name: "Singapore",
        google_address: "Singapore",
        location_latitude: "1.352083",
        location_longitude: "103.81983600000001",
        country: "Singapore",
        area: "Singapore",
        address: "Singapore",
        website: "http://app.singapore.sg/",
        url:
          "https://maps.google.com/?q=Singapore&ftid=0x31da11238a8b9375:0x887869cf52abf5c4",
        email: "export@vergersmekong.com.vn",
        no_address: true
      },
      {
        name: "Thailand",
        google_address: "Thailand",
        location_latitude: "15.870032",
        location_longitude: "100.99254100000007",
        country: "Thailand",
        area: "Thailand",
        address: "Thailand",
        website: "http://www.thaigov.go.th/",
        url:
          "https://maps.google.com/?q=Thailand&ftid=0x304d8df747424db1:0x9ed72c880757e802",
        email: "export@vergersmekong.com.vn",
        no_address: true
      },
      {
        name: "United Arab Emirates",
        google_address: "United Arab Emirates",
        location_latitude: "23.424076",
        location_longitude: "53.84781799999996",
        country: "United Arab Emirates",
        area: "United Arab Emirates",
        address: "United Arab Emirates",
        website: "http://www.government.ae/",
        url:
          "https://maps.google.com/?q=United+Arab+Emirates&ftid=0x3e5e48dfb1ab12bd:0x33d32f56c0080aa7",
        email: "export@vergersmekong.com.vn",
        no_address: true
      }
    ]
  };

var mapOptions = {
  zoom: 7,
  center: new google.maps.LatLng(10.1069139, 105.70857460000002),
  mapTypeId: google.maps.MapTypeId.ROADMAP,

  mapTypeControl: false,
  mapTypeControlOptions: {
    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
    position: google.maps.ControlPosition.LEFT_CENTER
  },
  panControl: false,
  panControlOptions: {
    position: google.maps.ControlPosition.TOP_RIGHT
  },
  zoomControl: true,
  zoomControlOptions: {
    position: google.maps.ControlPosition.RIGHT_BOTTOM
  },
  scrollwheel: false,
  scaleControl: false,
  scaleControlOptions: {
    position: google.maps.ControlPosition.TOP_LEFT
  },
  streetViewControl: true,
  streetViewControlOptions: {
    position: google.maps.ControlPosition.LEFT_TOP
  },
  styles: [
    {
      featureType: "administrative.country",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "administrative.province",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "administrative.locality",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "administrative.neighborhood",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "administrative.land_parcel",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "landscape.man_made",
      elementType: "all",
      stylers: [
        {
          visibility: "simplified"
        }
      ]
    },
    {
      featureType: "landscape.natural.landcover",
      elementType: "all",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "landscape.natural.terrain",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.business",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.government",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.medical",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.park",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.park",
      elementType: "labels",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.place_of_worship",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.school",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "poi.sports_complex",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "labels",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "road.highway.controlled_access",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "road.arterial",
      elementType: "all",
      stylers: [
        {
          visibility: "simplified"
        }
      ]
    },
    {
      featureType: "road.local",
      elementType: "all",
      stylers: [
        {
          visibility: "simplified"
        }
      ]
    },
    {
      featureType: "transit.line",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "transit.station.airport",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "transit.station.bus",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "transit.station.rail",
      elementType: "all",
      stylers: [
        {
          visibility: "off"
        }
      ]
    },
    {
      featureType: "water",
      elementType: "all",
      stylers: [
        {
          visibility: "on"
        }
      ]
    },
    {
      featureType: "water",
      elementType: "labels",
      stylers: [
        {
          visibility: "off"
        }
      ]
    }
  ]
};
var marker;
mapObject = new google.maps.Map(
  document.getElementById("map_right_listing"),
  mapOptions
);
for (var key in markersData)
  markersData[key].forEach(function(item) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(
        item.location_latitude,
        item.location_longitude
      ),
      map: mapObject,
      icon: "images/map-marker.png"
    });

    if ("undefined" === typeof markers[key]) markers[key] = [];
    markers[key].push(marker);
    google.maps.event.addListener(marker, "click", function() {
      closeInfoBox();
      getInfoBox(item).open(mapObject, this);
      mapObject.setCenter(
        new google.maps.LatLng(item.location_latitude, item.location_longitude)
      );
    });
  });

new MarkerClusterer(mapObject, markers[key]);

function hideAllMarkers() {
  for (var key in markers)
    markers[key].forEach(function(marker) {
      marker.setMap(null);
    });
}

function closeInfoBox() {
  $("div.infoBox").remove();
}

function getInfoBox(item) {
  return new InfoBox({
    content:
      '<div class="marker_info text-center" id="marker_info">' +
      "<span>" +
      
      "<h3>" +
      item.area +
      "</h3>" +
      '<div class="marker-address">' +
      item.address +
      "</div>" +
      '<a href="mailto:' +
      item.email +
      '" class="btn_infobox_phone">' +
      item.email +
      "</a>" +
      "</span>" +
      "</div>",
    disableAutoPan: false,
    maxWidth: 0,
    pixelOffset: new google.maps.Size(30, 60),
    closeBoxMargin: "",
    closeBoxURL: "images/close_infobox.png",
    isHidden: false,
    alignBottom: true,
    pane: "floatPane",
    enableEventPropagation: true
  });
}
function onHtmlClick(location_type, key) {
  google.maps.event.trigger(markers[location_type][key], "click");
}
