var htmlElement = document.documentElement;
var bodyElement = document.body;
var pageOverlay = document.getElementById("overlay");

var FOODY = {
  // _API: "https://publicapi.vienthonga.vn",
  // _URL: "https://vienthonga.vn"
};

var Layout = (function() {
  var setUserAgent = function() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
      bodyElement.classList.add("window-phone");
      return;
    }

    if (/android/i.test(userAgent)) {
      bodyElement.classList.add("android");
      return;
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      bodyElement.classList.add("ios");
      return;
    }
  };

  var viewportHeight = function() {
    // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
    let vh = window.innerHeight * 0.01;
    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty("--vh", `${vh}px`);

    // We listen to the resize event
    window.addEventListener("resize", () => {
      // We execute the same script as before
      let vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty("--vh", `${vh}px`);
    });
  };

  var scrollToTop = function() {
    var scrollTop = $("#scrollToTop");

    $(window).on("scroll", function() {
      if ($(window).scrollTop() > 200) {
        scrollTop.addClass("active");
      } else {
        scrollTop.removeClass("active");
      }
    });

    scrollTop.click(function() {
      $("body, html").animate(
        {
          scrollTop: 0
        },
        500
      );
    });
  };

  return {
    init: function() {
      setUserAgent();
      scrollToTop();
      viewportHeight();
    }
  };
})();

$(document).ready(function() {
  Layout.init();

  // .swiper-product

  /*  var swiper = new Swiper(".swiper-product", {
    slidesPerView: 3,
    spaceBetween: 20,
    lazy: {
      loadPrevNext: true
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    breakpoints: {
      640: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      1200: {
        slidesPerView: 2,
        spaceBetween: 20
      }
    }
  }); */
});

$(document).ready(function() {});

// Function for block height 100%
function height_line(height_object, height_donor) {
  height_object.height(height_donor.height());
  height_object.css({
    "line-height": height_donor.height() + "px"
  });
}
/* ---------------------------------------------
     Nav panel classic
     --------------------------------------------- */

var desktop_nav = $(".desktop-nav");

function init_classic_menu_resize() {
  // Mobile menu max height

  desktop_nav.show();
}

function init_classic_menu() {
  height_line($(".inner-nav > ul > li > a"), $(".main-nav"));

  // Transpaner menu

  if ($(".main-nav").hasClass("transparent")) {
    $(".main-nav").addClass("js-transparent");
  }

  $(window).scroll(function() {
    if ($(window).scrollTop() > 10) {
    } else {
    }
  });

  // Mobile menu toggle

  // Sub menu

  var mnHasSub = $(".mn-has-sub");
  var mnThisLi;

  $(".mobile-on .mn-has-sub")
    .find(".fa:first")
    .removeClass("fa-angle-right")
    .addClass("fa-angle-down");

  mnHasSub.click(function() {
    if ($(".main-nav").hasClass("mobile-on")) {
      mnThisLi = $(this).parent("li:first");
      if (mnThisLi.hasClass("js-opened")) {
        mnThisLi.find(".mn-sub:first").slideUp(function() {
          mnThisLi.removeClass("js-opened");
          mnThisLi
            .find(".mn-has-sub")
            .find(".fa:first")
            .removeClass("fa-angle-up")
            .addClass("fa-angle-down");
        });
      } else {
        $(this)
          .find(".fa:first")
          .removeClass("fa-angle-down")
          .addClass("fa-angle-up");
        mnThisLi.addClass("js-opened");
        mnThisLi.find(".mn-sub:first").slideDown();
      }

      return false;
    } else {
    }
  });

  mnThisLi = mnHasSub.parent("li");
  mnThisLi.hover(
    function() {
      if (!$(".main-nav").hasClass("mobile-on")) {
        $(this)
          .find(".mn-sub:first")
          .stop(true, true)
          .fadeIn("fast");
      }
    },
    function() {
      if (!$(".main-nav").hasClass("mobile-on")) {
        $(this)
          .find(".mn-sub:first")
          .stop(true, true)
          .delay(100)
          .fadeOut("fast");
      }
    }
  );

  /* Keyboard navigation for main menu */

  $(".inner-nav a").focus(function() {
    if (!$(".main-nav").hasClass("mobile-on")) {
      $(this)
        .parent("li")
        .parent()
        .children()
        .find(".mn-sub:first")
        .stop(true, true)
        .delay(100)
        .fadeOut("fast");
    }
  });

  $(".inner-nav a")
    .first()
    .keydown(function(e) {
      if (!$(".main-nav").hasClass("mobile-on")) {
        if (e.shiftKey && e.keyCode == 9) {
          $(this)
            .parent("li")
            .find(".mn-sub:first")
            .stop(true, true)
            .delay(100)
            .fadeOut("fast");
        }
      }
    });

  $(".mn-sub li:last a").keydown(function(e) {
    if (!$(".main-nav").hasClass("mobile-on")) {
      if (!e.shiftKey && e.keyCode == 9) {
        $(this)
          .parent("li")
          .parent()
          .stop(true, true)
          .delay(100)
          .fadeOut("fast");
      }
    }
  });

  $(document).keydown(function(e) {
    if (!$(".main-nav").hasClass("mobile-on")) {
      if (e.keyCode == 27) {
        if (
          mnHasSub
            .parent("li")
            .find(".mn-sub:first li .mn-sub")
            .is(":visible")
        ) {
          mnHasSub
            .parent("li")
            .find(".mn-sub:first li .mn-sub")
            .stop(true, true)
            .delay(100)
            .fadeOut("fast");
        } else {
          mnHasSub
            .parent("li")
            .find(".mn-sub:first")
            .stop(true, true)
            .delay(100)
            .fadeOut("fast");
        }
      }
    }
  });

  mnHasSub.focus(function() {
    if (!$(".main-nav").hasClass("mobile-on")) {
      $(this)
        .parent("li")
        .find(".mn-sub:first")
        .stop(true, true)
        .fadeIn("fast");
    }
  });
}

$(document).ready(function() {
  $(window).trigger("resize");
  init_classic_menu();
});

$(window).resize(function() {
  init_classic_menu_resize();
});

(function($) {
  "use strict";
  var Search = function(element, options) {
    this.$element = $(element);
    this.options = $.extend(true, {}, $.fn.search.defaults, options);
    this.init();
  };
  Search.VERSION = "1.0.0";
  Search.prototype.init = function() {
    var _this = this;
    this.pressedKeys = [];
    this.ignoredKeys = [];
    this.$searchField = this.$element.find(this.options.searchField);
    this.$closeButton = this.$element.find(this.options.closeButton);
    this.$suggestions = this.$element.find(this.options.suggestions);
    this.$brand = this.$element.find(this.options.brand);
    this.$searchField.on("keyup", function(e) {
      _this.$suggestions && _this.$suggestions.html($(this).val());
    });
    this.$searchField.on("keyup", function(e) {
      _this.options.onKeyEnter &&
        _this.options.onKeyEnter(_this.$searchField.val());
      if (e.keyCode == 13) {
        e.preventDefault();
        _this.options.onSearchSubmit &&
          _this.options.onSearchSubmit(_this.$searchField.val());
      }
      if ($("body").hasClass("overlay-disabled")) {
        return 0;
      }
    });
    this.$closeButton.on("click", function() {
      _this.toggleOverlay("hide");
    });
    this.$element.on("click", function(e) {
      if ($(e.target).data("pages") == "search") {
        _this.toggleOverlay("hide");
      }
    });
    $(document).on("keypress.pg.search", function(e) {
      _this.keypress(e);
    });
    $(document).on("keyup", function(e) {
      if (_this.$element.is(":visible") && e.keyCode == 27) {
        _this.toggleOverlay("hide");
      }
    });
  };
  Search.prototype.keypress = function(e) {
    e = e || event;
    var nodeName = e.target.nodeName;
    if (
      $("body").hasClass("overlay-disabled") ||
      $(e.target).hasClass("js-input") ||
      nodeName == "INPUT" ||
      nodeName == "TEXTAREA"
    ) {
      return;
    }
    if (
      e.which !== 0 &&
      e.charCode !== 0 &&
      !e.ctrlKey &&
      !e.metaKey &&
      !e.altKey &&
      e.keyCode != 27
    ) {
      this.toggleOverlay("show", String.fromCharCode(e.keyCode | e.charCode));
    }
  };
  Search.prototype.toggleOverlay = function(action, key) {
    var _this = this;
    if (action == "show") {
      this.$element.removeClass("hide");
      this.$element.fadeIn("fast");
      if (!this.$searchField.is(":focus")) {
        this.$searchField.val(key);
        setTimeout(
          function() {
            this.$searchField.focus();
            var tmpStr = this.$searchField.val();
            this.$searchField.val("");
            this.$searchField.val(tmpStr);
          }.bind(this),
          10
        );
      }
      this.$element.removeClass("closed");
      this.$brand.toggleClass("invisible");
      $(document).off("keypress.pg.search");
    } else {
      this.$element.fadeOut("fast").addClass("closed");
      this.$searchField.val("").blur();
      setTimeout(
        function() {
          if (this.$element.is(":visible")) {
            this.$brand.toggleClass("invisible");
          }
          $(document).on("keypress.pg.search", function(e) {
            _this.keypress(e);
          });
        }.bind(this),
        10
      );
    }
  };
  function Plugin(option) {
    return this.each(function() {
      var $this = $(this);
      var data = $this.data("pg.search");
      var options = typeof option == "object" && option;
      if (!data) {
        $this.data("pg.search", (data = new Search(this, options)));
      }
      if (typeof option == "string") data[option]();
    });
  }
  var old = $.fn.search;
  $.fn.search = Plugin;
  $.fn.search.Constructor = Search;
  $.fn.search.defaults = {
    searchField: '[data-search="searchField"]',
    closeButton: '[data-search="closeButton"]',
    suggestions: '[data-search="suggestions"]',
    brand: '[data-search="brand"]'
  };
  $.fn.search.noConflict = function() {
    $.fn.search = old;
    return this;
  };
  $(document).on("click.pg.search.data-api", '[data-toggle="search"]', function(
    e
  ) {
    var $this = $(this);
    var $target = $('[data-pages="search"]');
    if ($this.is("a")) e.preventDefault();
    $target.data("pg.search").toggleOverlay("show");
  });
})(window.jQuery);
(function($) {
  "use strict";
})(window.jQuery);

(function($) {
  "use strict";
  $(document).ready(function() {
    $('[data-pages="search"]').search({
      searchField: "#overlay-search",
      closeButton: ".overlay-close",
      suggestions: "#overlay-suggestions",
      brand: ".brand",
      onSearchSubmit: function(searchString) {
        console.log("Search for: " + searchString);
      },
      onKeyEnter: function(searchString) {
        console.log("Live search for: " + searchString);
        var searchField = $("#overlay-search");
        var searchResults = $(".search-results");
        clearTimeout($.data(this, "timer"));
        searchResults.fadeOut("fast");
        var wait = setTimeout(function() {
          searchResults.find(".result-name").each(function() {
            if (searchField.val().length != 0) {
              $(this).html(searchField.val());
              searchResults.fadeIn("fast");
            }
          });
        }, 500);
        $(this).data("timer", wait);
      }
    });
  });
})(window.jQuery);

$(window).on("load", function() {});

function getAll(selector) {
  return Array.prototype.slice.call(document.querySelectorAll(selector), 0);
}

var rootEl = document.documentElement;
var $modals = getAll(".modal");
var $modalButtons = getAll(".modal-button");
var $modalCloses = getAll(
  ".modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button"
);

if ($modalButtons.length > 0) {
  $modalButtons.forEach(function($el) {
    $el.addEventListener("click", function(e) {
      e.preventDefault();
      var target = $el.dataset.target;
      openModal(target);
    });
  });
}

if ($modalCloses.length > 0) {
  $modalCloses.forEach(function($el) {
    $el.addEventListener("click", function() {
      closeModals();
    });
  });
}

function openModal(target) {
  var $target = document.getElementById(target);
  rootEl.classList.add("is-clipped");
  $target.classList.add("is-active");
}

function closeModals() {
  rootEl.classList.remove("is-clipped");
  $modals.forEach(function($el) {
    $el.classList.remove("is-active");
  });
}

document.addEventListener("keydown", function(event) {
  var e = event || window.event;
  if (e.keyCode === 27) {
    closeModals();
    closeDropdowns();
  }
});
//CLOSE $.magnificPopup.close()

(function($) {
  var defaults = {
      topSpacing: 0,
      bottomSpacing: 0,
      className: "is-sticky",
      wrapperClassName: "sticky-wrapper",
      center: false,
      getWidthFrom: ""
    },
    $window = $(window),
    $document = $(document),
    sticked = [],
    windowHeight = $window.height(),
    scroller = function() {
      var scrollTop = $window.scrollTop(),
        documentHeight = $document.height(),
        dwh = documentHeight - windowHeight,
        extra = scrollTop > dwh ? dwh - scrollTop : 0;

      for (var i = 0; i < sticked.length; i++) {
        var s = sticked[i],
          elementTop = s.stickyWrapper.offset().top,
          etse = elementTop - s.topSpacing - extra;

        if (scrollTop <= etse) {
          if (s.currentTop !== null) {
            s.stickyElement.css("position", "").css("top", "");
            s.stickyElement.parent().removeClass(s.className);
            s.currentTop = null;
          }
        } else {
          var newTop =
            documentHeight -
            s.stickyElement.outerHeight() -
            s.topSpacing -
            s.bottomSpacing -
            scrollTop -
            extra;
          if (newTop < 0) {
            newTop = newTop + s.topSpacing;
          } else {
            newTop = s.topSpacing;
          }
          if (s.currentTop != newTop) {
            s.stickyElement.css("position", "fixed").css("top", newTop);

            if (typeof s.getWidthFrom !== "undefined") {
              s.stickyElement.css("width", $(s.getWidthFrom).width());
            }

            s.stickyElement.parent().addClass(s.className);
            s.currentTop = newTop;
          }
        }
      }
    },
    resizer = function() {
      windowHeight = $window.height();
    },
    methods = {
      init: function(options) {
        var o = $.extend(defaults, options);
        return this.each(function() {
          var stickyElement = $(this);

          var stickyId = stickyElement.attr("id");
          var wrapper = $("<div></div>")
            .attr("id", stickyId + "-sticky-wrapper")
            .addClass(o.wrapperClassName);
          stickyElement.wrapAll(wrapper);

          if (o.center) {
            stickyElement
              .parent()
              .css({
                width: stickyElement.outerWidth(),
                marginLeft: "auto",
                marginRight: "auto"
              });
          }

          if (stickyElement.css("float") == "right") {
            stickyElement
              .css({ float: "none" })
              .parent()
              .css({ float: "right" });
          }

          var stickyWrapper = stickyElement.parent();
          stickyWrapper.css("height", stickyElement.outerHeight());
          sticked.push({
            topSpacing: o.topSpacing,
            bottomSpacing: o.bottomSpacing,
            stickyElement: stickyElement,
            currentTop: null,
            stickyWrapper: stickyWrapper,
            className: o.className,
            getWidthFrom: o.getWidthFrom
          });
        });
      },
      update: scroller
    };

  // should be more efficient than using $window.scroll(scroller) and $window.resize(resizer):
  if (window.addEventListener) {
    window.addEventListener("scroll", scroller, false);
    window.addEventListener("resize", resizer, false);
  } else if (window.attachEvent) {
    window.attachEvent("onscroll", scroller);
    window.attachEvent("onresize", resizer);
  }

  $.fn.sticky = function(method) {
    if (methods[method]) {
      return methods[method].apply(
        this,
        Array.prototype.slice.call(arguments, 1)
      );
    } else if (typeof method === "object" || !method) {
      return methods.init.apply(this, arguments);
    } else {
      $.error("Method " + method + " does not exist on jQuery.sticky");
    }
  };
  $(function() {
    setTimeout(scroller, 0);
  });
})(jQuery);

$(document).ready(function() {});

(function($) {
  $.fn.touchwipe = function(settings) {
    var config = {
      min_move_x: 20,
      min_move_y: 20,
      wipeLeft: function() {},
      wipeRight: function() {},
      wipeUp: function() {},
      wipeDown: function() {},
      preventDefaultEvents: true
    };

    if (settings) $.extend(config, settings);

    this.each(function() {
      var startX;
      var startY;
      var isMoving = false;

      function cancelTouch() {
        this.removeEventListener("touchmove", onTouchMove);
        startX = null;
        isMoving = false;
      }

      function onTouchMove(e) {
        if (config.preventDefaultEvents) {
          e.preventDefault();
        }
        if (isMoving) {
          var x = e.touches[0].pageX;
          var y = e.touches[0].pageY;
          var dx = startX - x;
          var dy = startY - y;
          if (Math.abs(dx) >= config.min_move_x) {
            cancelTouch();
            if (dx > 0) {
              config.wipeLeft();
            } else {
              config.wipeRight();
            }
          } else if (Math.abs(dy) >= config.min_move_y) {
            cancelTouch();
            if (dy > 0) {
              config.wipeDown();
            } else {
              config.wipeUp();
            }
          }
        }
      }

      function onTouchStart(e) {
        if (e.touches.length == 1) {
          startX = e.touches[0].pageX;
          startY = e.touches[0].pageY;
          isMoving = true;
          this.addEventListener("touchmove", onTouchMove, false);
        }
      }
      if ("ontouchstart" in document.documentElement) {
        this.addEventListener("touchstart", onTouchStart, false);
      }
    });

    return this;
  };
})(jQuery);

$(document).ready(function() {
  var introPanel = $(".intro-item");
  var introPanelItem = $(".intro-item-w");

  introPanelItem.each(function(i, v) {
    var ele = $(".intro-item-w")[i];

    $(this).on("click", function(e) {
      if ($(this).hasClass("is-active")) return;

      introPanelItem.removeClass("is-active");
      $(this).addClass("is-active");
      introPanel.data("slide", i );
      introPanel.removeClass("is-active-2 is-active-0 is-active-1");
      introPanel.addClass("is-active-" + i);
    });

  });

  $("#intro-item").touchwipe({
    wipeLeft: function() {
      var ac = $(".intro-item").data("slide") + 1;
      if(ac <3){
        $(".intro-item-w")[ac].click();
      }
    },
    wipeRight: function() {
      var ac = $(".intro-item").data("slide") - 1;
      if(ac >= 0){
        $(".intro-item-w")[ac].click();
      }
    },
    min_move_x: 20,
    min_move_y: 20,
    preventDefaultEvents: true
  });
});
