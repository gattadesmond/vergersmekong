

//GET VARIABLES
$(window).on("ready load resizeEnd", function() {
  var $articleContent = $(".article-content");
   var panelHeight = 54;
   var articleOffset = $articleContent.offset();
 

  //blog Header height
  window.headerHeight = $(".blog-header").height();


  window.articleHeight = $articleContent.height();
  window.windowHeight = $(window).height();
  window.articleOffsetTop = articleOffset.top - panelHeight;
});

//ON SCROLL
$(window).on("scroll ready load resize resizeEnd", function(event) {
  if (window.isMobile) return;
  var $body = $('body');
  var scrollTop = $(window).scrollTop(),

    translateTitle = scrollTop / 2,
    translateBackground = scrollTop / 3,
    scale = scrollTop / window.headerHeight,
    backgroundScale = 1, // + scale / 10
    titleScale = 1 - scale * 0.1,
    titleOpacity = 1 - scale,
    scrollProgress =
      (scrollTop - window.articleOffsetTop) /
      (window.articleHeight - window.windowHeight / 6);

  // HEADER ANIMATION
  if (scrollTop < window.headerHeight) {
    //shift the title
    $(".blog-header-ani").css({
      "-webkit-transform":
        "translateY(" + translateTitle + "px) scale(" + titleScale + ")",
      transform:
        "translateY(" + translateTitle + "px) scale(" + titleScale + ")",
      opacity: titleOpacity
    });

    //translate and scale
    $(".blog-header-background").css({
      "-webkit-transform":
        "scale(" +
        backgroundScale +
        ") translateY(" +
        translateBackground +
        "px)",
      transform:
        "scale(" +
        backgroundScale +
        ") translateY(" +
        translateBackground +
        "px)",
      opacity: 1 - scale / 1.2
    });

    $body.removeClass("contentPart").removeClass("bottomPart");
  } else if (scrollTop >= window.headerHeight) {
    $body.addClass("contentPart").removeClass("bottomPart");
  }

  if (scrollProgress >= 0 && scrollProgress <= 1) {
    $(".blog-panel .progress")
      .removeClass("hide")
      .css("width", 100 * scrollProgress + "%");
  } else if (scrollProgress > 1) {
    $body.removeClass("contentPart"); //.addClass('bottomPart');
  } else if (scrollProgress < 0) {
    $(".blog-panel .progress")
      .addClass("hide")
      .css("width", "0%");
  }
});

